<?php

namespace Starapple\Utils;

class TwigDataCollector {
	
	/**
	 * @var \Starapple\Utils\TwigDataCollector
	 */
	private static $inst = null;
	
	/**
	 * Data
	 * @var array 
	 */
	private $data = null;
	
	private function __construct()
	{
		$this->data = array();
	}
	
	/**
	 * @return \Starapple\Utils\TwigDataCollector
	 */
	public static function getInstance()
	{
		if(self::$inst === null)
		{
			self::$inst = new TwigDataCollector();		
		}
		
		return self::$inst;
	}

	/**
	 * Voeg een waarde toe
	 * @param atring $key
	 * @param arry/string $value
	 */
	public function set($key, $value)
	{
		$this->data[$key] = $value;
	}
	
	/**
	 * Verwijder een waarde
	 * @param string $key
	 */
	public function remove($key)
	{
		if(array_key_exists($key, $this->data)) unset($this->data, $key);
	}
	
	/**
	 * Returns all data
	 * @return type
	 */
	public function getAll() {
		return $this->data;
	}
}
