<?php
	namespace Starapple\Model;

	use Silex\Application;
	use Symfony\Component\HttpFoundation\Request;

	class PostModel extends BaseModel
	{
		public function __construct(Request $request, Application $app)
		{
			$allowed =array('id','author_id','title','message','created','modified');

			$type = array();
			$type['id']			= \PDO::PARAM_INT;
			$type['author_id']	= \PDO::PARAM_INT;
			$type['title']		= \PDO::PARAM_STR;
			$type['message']	= \PDO::PARAM_STR;
			$type['created']	= "datetime";
			$type['password']	= "datetime";

			parent::__construct($request, $app, "post", $allowed, $type);
		}

		
		/**
		 * Haal alle posts op 
		 */
		public function getAll($authorId = null)
		{
			$sql = "SELECT p.*, a.firstname, a.lastname, a.email FROM ".$this->table." AS p 
				
					INNER JOIN `author`AS a
					ON p.author_id = a.id ";
			
			if($authorId !== null && is_numeric($authorId)) $sql .= "WHERE p.author_id =:author_id ";
			
			$sql .= "ORDER BY p.author_id ASC, p.created DESC";
			
			$posts = $this->conn->prepare($sql);

			if($authorId !== null && is_numeric($authorId)){
				$posts->bindValue('author_id', $authorId);
			}
		
			$posts->execute();

			return $posts->fetchAll();	
		}
		
		/**
		 * Haal een post op, op basis van ID
		 * @param int $postId
		 * @return type
		 */
		public function getPostById($postId)
		{
			$sql = "SELECT p.*, a.firstname, a.lastname, a.email FROM ".$this->table." AS p 

					INNER JOIN `author`AS a
					ON p.author_id = a.id 
			
					WHERE p.id =:post_id

					LIMIT 1";

			$post = $this->conn->prepare($sql);
			$post->bindValue('post_id', $postId);
			$post->execute();

			$result = $post->fetch();

			return $result;	
		}
		
		
		/**
		 * 
		 * @param int $authorId
		 * @param int $postId
		 */
		public function getPost($authorId, $postId)
		{
			$result = null;
			
			if(is_numeric($authorId) && is_numeric($postId))
			{
				$sql = "SELECT p.* FROM ".$this->table." AS p 

						WHERE	p.author_id =:author_id  AND
								p.id =:post_id

						LIMIT 1";
			
				$post = $this->conn->prepare($sql);
				$post->bindValue('author_id', $authorId);
				$post->bindValue('post_id', $postId);
				$post->execute();
				
				$result = $post->fetch();
				
			}  else {
				$result = $this->getColumnNames();
				
			}	
			return $result;
		} 
		
		/**
		 * Insert een blog post
		 * @param int $authorId
		 */
		public function insertUpdate($authorId)
		{
			$data				= $this->getPostData();
			$data['author_id']	= $authorId;

			if($data['id'] == 'new')
			{
				unset($data['id']);
				$data['created']	= $this->getCurrentDateTime();
				$data['modified']	= $this->getCurrentDateTime();
				
				$this->conn->insert($this->table, $data, $this->type);
			}  else {
				
				$id = $data['id'];
				unset($data['id']);
				$data['modified'] = $this->getCurrentDateTime();
				
				$this->conn->update($this->table, $data, array('id' => $id));
			}
		}
		
		/**
		 * Verwijder een post
		 * @param type $authorId
		 * @param type $postId
		 */
		public function delete($authorId, $postId)
		{
			$this->conn->delete($this->table, array('author_id' => $authorId, 'id' => $postId));
		}
		
	}


