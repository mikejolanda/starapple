<?php
	namespace Starapple\Model;

	use Silex\Application;
	use Symfony\Component\HttpFoundation\Request;
	
	class AuthorModel extends BaseModel
	{
		public function __construct(Request $request, Application $app)
		{
			$allowed =array('firstname', 'lastname', 'email', 'password');
			
			$type = array();
			$type['id']			= \PDO::PARAM_INT;
			$type['firstname']	= \PDO::PARAM_STR;
			$type['lastname']	= \PDO::PARAM_STR;
			$type['email']		= \PDO::PARAM_STR;
			$type['password']	= \PDO::PARAM_STR;
			$type['created']	= "datetime";
			$type['password']	= "datetime";

			parent::__construct($request, $app, "author", $allowed, $type);
		}
		
		/**
		 * retrieve a list of all authors
		 */
		public function getAll()
		{
			$statement = $this->conn->prepare("SELECT * FROM ".$this->table." ORDER BY `lastname` ASC, `firstname` ASC");
			$statement->execute();
			return $statement->fetchAll();
		}   
		
		/**
		 * Sla een registratie op
		 * @param (array) 
		 */
		public function register()
		{
			$data = $this->getPostData();
			
			$data['password']	= password_hash($data['password'], PASSWORD_BCRYPT);
			$data['email']		= strtolower($data['email']);
			$data['created']	= $this->getCurrentDateTime();
			$data['modified']	= $this->getCurrentDateTime();

			if($this->getAuthorByEmail($data['email']) === false)
			{
				//Omdat er een unique indes ligt op het e-mail adres
				$this->conn->insert($this->table, $data, $this->type);
				return true;
			}  else {
				return false;
			}
		}
		
		/**
		 * Haal een auteur op, op basis van zijn e-mail adres
		 * @param string $email
		 */
		public function getAuthorByEmail($email = null)
		{
			$email = ($email === null)? strtolower($this->getPostData()['email']) : strtolower($email);

			$author = $this->conn->prepare("SELECT * FROM ".$this->table." WHERE `email` =:email");
			$author->bindValue('email', $email);
			$author->execute();

			return $author->fetch();	
		}

		/**
		 * Haal een author op, op basis van het Id
		 * @param int $authorId
		 */
		public function getAuthor($authorId)
		{
			$author = $this->conn->prepare("SELECT * FROM ".$this->table." WHERE `id` =:authorId");
			$author->bindValue('authorId', $authorId);
			$author->execute();
			
			return $author->fetch();	
		}
		
		
		
		
		//The following APIs are designed to be SAFE from SQL injections:
		//
		//    For Doctrine\DBAL\Connection#insert($table, $values, $types), Doctrine\DBAL\Connection#update($table, $values, $where, $types) 
		//    and Doctrine\DBAL\Connection#delete($table, $where, $types) only the array values of $values and $where. The table name and keys 
		//    of $values and $where are NOT escaped.
		//    Doctrine\DBAL\Query\QueryBuilder#setFirstResult($offset)
		//    Doctrine\DBAL\Query\QueryBuilder#setMaxResults($limit)
		//    Doctrine\DBAL\Platforms\AbstractPlatform#modifyLimitQuery($sql, $limit, $offset) for the $limit and $offset parameters.
		//
		//Consider ALL other APIs to be not safe for user-input:
		//
		//    Query methods on the Connection
		//    The QueryBuilder API
		//    The Platforms and SchemaManager APIs to generate and execute DML/DDL SQL statements
		//
		//To escape user input in those scenarios use the Connection#quote() method.
		
	}	