<?php
	namespace Starapple\Model;

	use Silex\Application;
	use Symfony\Component\HttpFoundation\Request;
	use Silex\Provider\DoctrineServiceProvider;
	
	class BaseModel
	{
	
		/**
		 * @var \Silex\Application 
		 */
		protected $app;
		
		/**
		 * @var Symfony\Component\HttpFoundation\Request 
		 */
		protected $request;
		
		/**
		 * @var \Silex\Provider\DoctrineServiceProvider
		 */
		protected $conn;

		/**
		 * @var String
		 * De tabel naam
		 */
		protected $table;

		/**
		 * @var array
		 * Toegestane velden
		 */
		protected $allowed;
		
		/**
		 * @var array
		 * Veld types
		 */
		protected $type;
		
		/**
		 * array type 
		 * Post variables
		 */
		protected $post;

		
		
		public function __construct(Request $request, Application $app, $table, $allowed, $type)
		{
			$this->request  = $request;
			
			$this->app		= $app;

			$this->conn		= $this->app['db'];

			$this->allowed	= $allowed;
			
			$this->type		= $type;
			
			$this->table	= $table;
			
			$this->post		= array();

			//Alle geposte variabelen opslaan en degene gebruiken die nodig zijn
			foreach($this->request->request->all() as $key  => $value)
			{
				//EEHM SCHIET MIJ MAAR LEK DIT BETEKENT DAT DOCTRIN OF SYMPHONY EEN DUBBEL CODERING UITVOERT
				if(in_array($key, $this->allowed)) $this->post[$key] = utf8_decode(urldecode($value));
			}

		}
		
		
		/**
		 * Get Column name
		 */
		protected function getColumnNames()
		{
			$tableColumns = array();
			
			$sm			= $this->conn->getSchemaManager();
			$columns	= $sm->listTableColumns($this->table);
			
			foreach($columns as $key => $value)
			{
				$tableColumns[$key] = ($key == 'id') ? 'new' : '';
			}
			
			return  $tableColumns;
		}
		
		/**
		 * return current datetime
		 * @return date
		 */
		public function getCurrentDateTime()
		{
			return date("Y-m-d H:i:s");
		}
		
		/**
		 * return current date
		 * @return date
		 */
		public function getCurrentDate()
		{
			return date("Y-m-d");
		}
		
		/**
		 * return current time
		 * @return date
		 */
		public function getCurrentTime()
		{
			return date("H:i:s");
		}
		
		public function getPostData()
		{
			return $this->post;
		}
		
		
	}
