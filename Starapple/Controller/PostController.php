<?php
	namespace Starapple\Controller;

	use Silex\Application;
	
	use Symfony\Component\HttpFoundation\Request;
	
	use Starapple\Model\AuthorModel;
	use Starapple\Model\PostModel;
	use Starapple\Utils\TwigDataCollector;
	
	Class PostController extends BaseController
	{
		public function __construct() {
		}
		
		
		/**
		 * Handel de index request af...
		 * @param Symfony\Component\HttpFoundation\Request $request
		 * @param Silex\Application $app
		 * @return type
		 */
		public function readPost(Request $request, Application $app, $id)
		{
			$postModel = new PostModel($request, $app);
			$post = $postModel->getPostById($id);
			
			if($post !== false)
			{
				TwigDataCollector::getInstance()->set('post', $post);
				TwigDataCollector::getInstance()->set('authors', (new AuthorModel($request, $app))->getAll());

				$this->getAuthorLoginInformation($app);

				return $app['twig']->render('blog.post.twig', TwigDataCollector::getInstance()->getAll());		
			}else
			{
				return $app->redirect('/');
			}
		}
		
		
		public function postEdit(Request $request, Application $app, $id)
		{
			//Alleen wanneer je ingelogd bent!
			if($app['session']->has('author'))
			{
				$postModel	= new PostModel($request, $app);
				$post		= $postModel->getPost($app['session']->get('author')['id'], $id);
				
				TwigDataCollector::getInstance()->set('post',  $post);
				
				return $app['twig']->render('blog.author.post.form.twig', TwigDataCollector::getInstance()->getAll());	 
			}else
			{
				return $app['twig']->render('blog.author.post.not.logged.in');	 
			}
		}
		
		public function postUpdateInsert(Request $request, Application $app)
		{
			if($app['session']->has('author'))
			{
				$authorId = $app['session']->get('author')['id'];
				
				$modelPost	= new PostModel($request, $app);
				$modelPost->insertUpdate($authorId);

				$modelAuthor = new AuthorModel($request, $app);
				
				//Haal de ingelogde info op en zet die door naar het template
				$this->getAuthorLoginInformation($app);
			
				TwigDataCollector::getInstance()->set('current_author', $modelAuthor->getAuthor($authorId));
				TwigDataCollector::getInstance()->set('authors', $modelAuthor->getAll());
				TwigDataCollector::getInstance()->set('posts', $modelPost->getAll($authorId));
				
				return $app['twig']->render('blog.author.posts.twig', TwigDataCollector::getInstance()->getAll());	 
				
			}  else {
				return $app['twig']->render('blog.author.post.not.logged.in.twig');	 
			}
		}
		
		/**
		 * Verwijder een post
		 * @param Request $request
		 * @param Application $app
		 * @param int $id
		 */
		public function postDelete(Request $request, Application $app, $id)
		{
			if($app['session']->has('author') && is_numeric($id))
			{
				$authorId = $app['session']->get('author')['id'];
				
				$modelPost	= new PostModel($request, $app);
				$modelPost->delete($authorId, $id);

				$modelAuthor = new AuthorModel($request, $app);
				
				//Haal de ingelogde info op en zet die door naar het template
				$this->getAuthorLoginInformation($app);
			
				TwigDataCollector::getInstance()->set('current_author', $modelAuthor->getAuthor($authorId));
				TwigDataCollector::getInstance()->set('authors', $modelAuthor->getAll());
				TwigDataCollector::getInstance()->set('posts', $modelPost->getAll($authorId));
				
				return $app['twig']->render('blog.author.posts.twig', TwigDataCollector::getInstance()->getAll());	 
				
			}  else 
			{
				return $app['twig']->render('blog.author.post.not.logged.in.twig');	 
			}
		}
		
	}
