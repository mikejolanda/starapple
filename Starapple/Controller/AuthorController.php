<?php
	namespace Starapple\Controller;

	use Silex\Application;
	use Symfony\Component\HttpFoundation\Request;
	use Starapple\Model\AuthorModel;
	use Starapple\Model\PostModel;
	use Starapple\Utils\TwigDataCollector;
	
	Class AuthorController extends BaseController
	{
		public function __construct() {
		}
		
		/**
		 * Registreer een author...
		 * @return template
		 */
		public function authorRegister(Request $request, Application $app)
		{
			
			$model = new AuthorModel($request, $app);
			
			if($model->register())
			{
				return $app['twig']->render('blog.new.author.succes.twig');
			}else
			{
				//Voor veiligheids redenen
				$post = $model->getPostData();
				$post['password'] = '';
				$post['repeat_password'] = '';
				
				TwigDataCollector::getInstance()->set('message', "Er bestaat al een auteur met dit e-mail adres!");
				TwigDataCollector::getInstance()->set('author', $post);
				
				return $app['twig']->render('blog.new.author.twig', TwigDataCollector::getInstance()->getAll() );	
			}
		}
		
		/**
		 * Log een auteur in
		 * @param Symfony\Component\HttpFoundation\Request $request
		 * @param Silex\Application $app
		 * @return type
		 */
		public function authorLogin(Request $request, Application $app)
		{
			$this->app		= $app;
			$model	= new AuthorModel($request, $app);
			
			$author			= $model->getAuthorByEmail();

			$loggedIn = false;

			if($author !== false)
			{
				//Check of het wachtwoord correct is
				$loggedIn = password_verify($model->getPostData()['password'], $author['password']);
			}
			
			TwigDataCollector::getInstance()->set('authors', $model->getAll());
			

			if($loggedIn)
			{
				$app['session']->set('author' , $author);
				$app['session']->set('author_login_time' , $model->getCurrentTime());
				$app['session']->set('author_login_date' , $model->getCurrentDate());

				TwigDataCollector::getInstance()->set('author', $author);
				TwigDataCollector::getInstance()->set('author_login_time', $app['session']->get('author_login_time'));
				TwigDataCollector::getInstance()->set('author_login_date', $app['session']->get('author_login_date'));
				
				return $app['twig']->render('blog.login.succes.twig', TwigDataCollector::getInstance()->getAll());	
			}  else {
				
				TwigDataCollector::getInstance()->set('message', "Inloggen mislukt, wachtwoord e-mail combinatie is niet gevonden.");

				return $app['twig']->render('blog.login.failed.twig', TwigDataCollector::getInstance()->getAll());	
			}
		}
		
		
		/**
		 * Haal alle posts van een specifiek auteur op
		 * @param Symfony\Component\HttpFoundation\Request $request
		 * @param Silex\Application $app
		 * @return type
		 */
		public function authorOverview(Request $request, Application $app, $id)
		{
			$modelAuthor	= new AuthorModel($request, $app);
			$modelPosts		= new PostModel($request, $app);
			
			//Haal de ingelogde info op en zet die door naar het template
			$this->getAuthorLoginInformation($app);
			
			$author = $modelAuthor->getAuthor($id);
			
			if($author !== false)
			{
				TwigDataCollector::getInstance()->set('current_author', $author);
				TwigDataCollector::getInstance()->set('authors', $modelAuthor->getAll());
				TwigDataCollector::getInstance()->set('posts', $modelPosts->getAll($id));

				return $app['twig']->render('blog.author.main.twig', TwigDataCollector::getInstance()->getAll());	
			}else
			{
				return $app->redirect('/');
			}
		}
	}

