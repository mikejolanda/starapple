<?php
	namespace Starapple\Controller;
	
	use Starapple\Utils\TwigDataCollector;

	class BaseController {
	
		/**
		 * Check of auteur is ingelogged en render de info naar de template
		 */
		public function getAuthorLoginInformation(\Silex\Application $app)
		{
			if($app['session']->has('author'))
			{
				TwigDataCollector::getInstance()->set('author', $app['session']->get('author'));
				TwigDataCollector::getInstance()->set('author_login_time', $app['session']->get('author_login_time'));
				TwigDataCollector::getInstance()->set('author_login_date', $app['session']->get('author_login_date'));
			}
		}
	}
