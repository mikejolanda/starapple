<?php
	namespace Starapple\Controller;

	use Silex\Application;
	use Symfony\Component\HttpFoundation\Request;


	use Starapple\Model\AuthorModel;
	use Starapple\Model\PostModel;
	use Starapple\Model\QuoteModel;
	use Starapple\Utils\TwigDataCollector;
	
	Class IndexController extends BaseController
	{
		public function __construct() {
		}
		
		/**
		 * Handel de index request af...
		 * @param Symfony\Component\HttpFoundation\Request $request
		 * @param Silex\Application $app
		 * @return type
		 */
		public function handleIndexRequest(Request $request, Application $app)
		{
			TwigDataCollector::getInstance()->set('posts',	(new PostModel($request, $app))->getAll());
			TwigDataCollector::getInstance()->set('authors',(new AuthorModel($request, $app))->getAll());
			TwigDataCollector::getInstance()->set('quote',	(new QuoteModel($request, $app))->getRandomQuote());

			$this->getAuthorLoginInformation($app);

			return $app['twig']->render('blog.quote.twig', TwigDataCollector::getInstance()->getAll());			
		}
		
		/**
		 * Uitloggen als auteur
		 * @param Request $request
		 * @param Application $app
		 * @return type
		 */
		public function authorLogout(Request $request, Application $app)
		{
			$app['session']->clear();
			return $app->redirect('/');
		}
		
		/**
		 * Render het registratie formulier
		 * @param Symfony\Component\HttpFoundation\Request $request
		 * @param Silex\Application $app
		 * @return type
		 */
		public function menuRegister(Request $request, Application $app)
		{
			return $app['twig']->render('blog.new.author.twig');			
		}
		
		/**
		 * Render het login formulier
		 * @param Symfony\Component\HttpFoundation\Request $request
		 * @param Silex\Application $app
		 * @return type
		 */
		public function menuLogin(Request $request, Application $app)
		{
			TwigDataCollector::getInstance()->set('authors', (new AuthorModel($request, $app))->getAll());
			return $app['twig']->render('blog.login.twig', TwigDataCollector::getInstance()->getAll());		
		}
	}